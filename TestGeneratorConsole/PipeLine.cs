﻿using System.Threading.Tasks.Dataflow;
using TestGeneratorLib;

namespace TestGeneratorConsole
{
    public class PipeLine(int maxReadingTask, int maxProcessingTask, int maxWritingTask, TestGenerator testGenerator)
    {
        private const string ExtensionCs = ".cs";

        private readonly struct StringPair(string name, string value)
        {
            public string Name { get; } = name;
            public string Value { get; } = value;
        }

        public async Task Process(string srcDir, string resDir)
        {
            if (!Directory.Exists(srcDir))
            {
                throw new ArgumentException();
            }

            if (!Directory.Exists(resDir))
            {
                Directory.CreateDirectory(resDir);
            }

            var readFiles = new TransformBlock<string, StringPair>(
                async path => new StringPair(Path.GetFileName(path), await FileRead(path)),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = maxReadingTask });

            var processFiles = new TransformBlock<StringPair, List<StringPair>>(
                FileProcess,
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = maxProcessingTask });

            var writeFiles = new ActionBlock<List<StringPair>>(
                async content => await FileWrite(content, resDir),
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = maxWritingTask });

            var linkOptions = new DataflowLinkOptions
            {
                PropagateCompletion = true
            };

            readFiles.LinkTo(processFiles, linkOptions);
            processFiles.LinkTo(writeFiles, linkOptions);
            foreach (var filePath in Directory.GetFiles(srcDir))
            {
                readFiles.Post(filePath);
            }

            readFiles.Complete();
            await writeFiles.Completion;
        }

        private async Task<List<StringPair>> FileProcess(StringPair srcFile)
        {
            var results = new List<StringPair>();

            var tests = await testGenerator.GenerateForFile(srcFile.Value);

            if (tests == null)
            {
                return results;
            }

            results.AddRange(from testContent in tests
                let resultName = srcFile.Name + '_' + testContent.NamespaceName + "_" + testContent.ClassName
                select new StringPair(resultName, testContent.GetCode()));
            return results;
        }

        private async Task<string> FileRead(string file)
        {
            using var sr = new StreamReader(file);
            var result = await sr.ReadToEndAsync();
            return result;
        }

        private async Task FileWrite(List<StringPair> files, string @out)
        {
            foreach (var filename in files)
            {
                var resultFilePath = @out + Path.DirectorySeparatorChar + filename.Name + ExtensionCs;
                Console.WriteLine(resultFilePath);
                await using var sw = new StreamWriter(resultFilePath);
                await sw.WriteAsync(filename.Value);
            }
        }
    }
}