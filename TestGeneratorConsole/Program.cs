﻿using TestGeneratorLib;

namespace TestGeneratorConsole
{
    internal static class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("src dir");
            var srcDir = Console.ReadLine();
            Console.WriteLine("out dir");
            var resDir = Console.ReadLine();
            Console.WriteLine("read threads");
            var maxReadingTask = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("proc threads");
            var maxProcessingTask = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("write threads");
            var maxWritingTask = Convert.ToInt32(Console.ReadLine());

            var pipeLine = new PipeLine(maxReadingTask, maxProcessingTask, maxWritingTask, new TestGenerator());
            await pipeLine.Process(srcDir, resDir);
        }
    }
}