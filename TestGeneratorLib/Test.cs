﻿using Microsoft.CodeAnalysis;

namespace TestGeneratorLib
{
    public class Test(string className, string namespaceName, SyntaxTree? tree)
    {
        public string ClassName { get; private set; } = className;
        public string NamespaceName { get; private set; } = namespaceName;

        public string? GetCode()
        {
            return tree?.ToString();
        }
    }
}
